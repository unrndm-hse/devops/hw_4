FROM ubuntu:21.04

ENV HOST=0.0.0.0
ENV PORT=5000
ENV DEBIAN_FRONTEND=noninteractive

EXPOSE 5000

RUN apt update && \
    apt install -y build-essential cmake python3-pip python3.9-full && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir flask

WORKDIR /cmake-with-flask

COPY ./cmake-with-flask .

RUN cmake . && make && ./bin/C

# create user with home dir and bash default shell
RUN useradd -ms /bin/bash techproguser

CMD python3.9 app.py --port "$PORT" --host "$HOST"
