FROM openjdk:8

WORKDIR /DostavimVse

COPY ./DostavimVse .

RUN ./mvnw -Pinit-base -DskipTests clean package
CMD java -jar ./target/dostavimvse-0.0.1-SNAPSHOT.jar && ./mvnw -Pweb-app -DskipTests clean package && java -jar ./target/dostavimvse-0.0.1-SNAPSHOT.jar --server.port=8080

