Homework 4
==========

[task_description](https://docs.google.com/document/d/1RmHK3CN8yC2n3HbKmk-bYpPKTXaGblvV0PcU9TBS-y8/edit)

Task 1
------
(5 points) Create Dockerfile for cmake-with-flask project (please, do not use multi-stage builds):
- [x] (1 point) Inside a cmake project should be built, launch is executed by bin/C
- [x] (2 points) A ‘techproguser’ user should be created
- [x] (2 points) Flask app launch should be stated as a launch command (testing system thinks that app starts on port 5000)

to run, execute the following:
```sh
docker build -t devops:hw-4-1 . && docker rm -f cmake-with-flask && docker run --name cmake-with-flask -p 5000:5000 devops:hw-4-1
```

Task 2
------
(5 points) In project root folder create docker-compose.yml file, where project from DostavimVse folder will be built, using Maven build system:
- [x] Database - MySQL, version 5.7
- [x] Java version - 8
- [x] You have to set launch of CREATE.sql file from scripts folder on the creation of database container
- [x] On the start of the web-app container a script, which fills the database with starting data, should be launched. Hint: web-project is built with spring-boot framework.
- [x] (One more hint) Web-application does not start without a database. You need to find a way of creating the database before the launch of the application.

[usefull link](https://stackoverflow.com/questions/42840576/springboot-unable-to-find-a-single-main-class-from-the-following-candidates)

### Note
Test's don't work

run with
```sh
docker compose down && docker compose build && docker compose up -d && docker compose logs -f
```
